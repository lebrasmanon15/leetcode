def addTwoNumbers(l1, l2):
    l1 = [str(i) for i in l1[-1:-len(l1)-1:-1]]
    l2 = [str(i) for i in l2[-1:-len(l2)-1:-1]]

    resultl1 = int("".join(l1))
    resultl2 = int("".join(l2))

    return [i for i in str((resultl1+resultl2))]


print(addTwoNumbers([2,4,3], [5,6,4]))