#Reverse type exercice
#My code 
import string

n = int(input())
alphabet = list(string.ascii_uppercase)
print(alphabet[n-1]*n)


#Winner's code
""" n = int(input())
print(chr(64+n)*n) """