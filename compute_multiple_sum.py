def compute_multiple_sum(n):
    return sum([i for i in range(1, n) if (i%3 == 0 or i%5 == 0 or i%7== 0)])


print(compute_multiple_sum(11))