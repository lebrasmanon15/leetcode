def convert(s, numRows):
    resultTab = [[] for i in range(numRows)]
    counter = 0
    if len(s) == 1:
        result = s
    elif numRows <= 2:
        while(counter < len(s)):
            for i in range(0, numRows):
                if counter <= len(s)-1:
                    resultTab[i].append(s[counter])
                    counter += 1
                else:
                    break
        result = "".join(["".join(elem) for elem in resultTab])
    else:
        while(counter < len(s)):
            for i in range(0, numRows):
                if counter <= len(s)-1:
                    resultTab[i].append(s[counter])
                    counter += 1
                else:
                    break
            for x in range(0, numRows - 2):
                if counter <= len(s)-1:
                    resultTab[-1-x-1].append(s[counter])
                    counter += 1
                else:
                    break
        result = "".join(["".join(elem) for elem in resultTab])
        
    return result


print(convert("Abcdefg", 2))
print(convert("PAYPALISHIRING",4))