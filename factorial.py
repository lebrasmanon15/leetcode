""" def factorial(n):
    return 1 if n == 1 else n*factorial(n-1) """

def factorial(n):
    result = 1 if n == 1 else n*factorial(n-1)
    return result

print(factorial(3))