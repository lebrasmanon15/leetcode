from math import floor

def findMedian(nums1, nums2):
    nums_merged = sorted(nums1 + nums2)
    if len(nums_merged) % 2 == 0:
        return (nums_merged[int((len(nums_merged)/2))-1] + nums_merged[int(len(nums_merged)/2)])/2 
    else:
        return nums_merged[floor(len(nums_merged)/2)]


print(findMedian([1,3], [2, 4]))

