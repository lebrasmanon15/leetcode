""" Given an input string s and a pattern p, implement regular expression matching with support for '.' and '*' where:

'.' Matches any single character.​​​​
'*' Matches zero or more of the preceding element.
The matching should cover the entire input string (not partial). """


def isMatch(s, p):
    if len(s) != len(p):
            result = False
    else:
        
        for i in range(len(s)):
            if s[i] == p[i] or p[i] == ".":
                result = True
            elif p[i] == "*" and (s[i-1] == s[i] or p[i-1] == "."):
                result = True
            else:
                result = False
                break
    return result


print(isMatch("ab", ".*"))

# Doesn't work with s="aab", p="c*a*b" expected True