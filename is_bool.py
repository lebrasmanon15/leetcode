def is_bool(i,j):
    return 1 in [i,j] or sum([i,j]) == 1

print(is_bool(1,3))
print(is_bool(5,4))
print(is_bool(-3, 4))
print(is_bool(7,-9))


