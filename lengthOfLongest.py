def lengthOfLongestSubstring(s):
    result = ""
    words = []
    if not s.isalpha():
        return len(s)
    else:
        for letter in s:
            if len(result) == 0 or letter not in result:
                result += letter
            else:
                words.append(result)
                result = letter
        return max([len(word) for word in words])


print(lengthOfLongestSubstring("abcabcbb"))
print(lengthOfLongestSubstring("bbbbb")) 
print(lengthOfLongestSubstring("pwwkew"))
print(lengthOfLongestSubstring("dvdf"))

