n = int(input())
answer = 0
def l(n):
    return 1 if n <= 1 else l(n-1) + l(n-2) + 1

print(l(n))
