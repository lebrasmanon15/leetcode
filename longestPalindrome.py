def longestPalindrome(s):
    test = ""
    final = s[0]
    for i in range(0,len(s)):
        test += s[i]
        for x in range(i+1,len(s)):
            test += s[x]
            if test == test[::-1] and len(test) > len(final):
                final = test
        test = ""
    return final


print(longestPalindrome("bb")) # expected "bb"
print(longestPalindrome("babad")) # expected "bab"





