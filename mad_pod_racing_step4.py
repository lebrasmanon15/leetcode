import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.
def distpod(x1, y1, x2, y2):
    return (math.sqrt((x2-x1)**2+(y2-y1)**2)) < 50 #rank 1754 with 50
boost = 0
# game loop
while True:
    # next_checkpoint_x: x position of the next check point
    # next_checkpoint_y: y position of the next check point
    # next_checkpoint_dist: distance to the next checkpoint
    # next_checkpoint_angle: angle between your pod orientation and the direction of the next checkpoint
    x, y, next_checkpoint_x, next_checkpoint_y, next_checkpoint_dist, next_checkpoint_angle = [int(i) for i in input().split()]
    opponent_x, opponent_y = [int(i) for i in input().split()]

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr, flush=True)


    # You have to output the target position
    # followed by the power (0 <= thrust <= 100)
    # i.e.: "x y thrust"
    
    if next_checkpoint_dist > 2000 and boost == 0:
        print(str(next_checkpoint_x) + " " + str(next_checkpoint_y) + " BOOST")
        boost = 1
    elif next_checkpoint_dist < 200 or next_checkpoint_angle > 80 or next_checkpoint_angle < -80 or distpod(x,y,opponent_x, opponent_y):
        print(str(next_checkpoint_x) + " " + str(next_checkpoint_y) + " 50")
    else:
        print(str(next_checkpoint_x) + " " + str(next_checkpoint_y) + " 100")
