import sys
import math

# This code automatically collects game data in an infinite loop.
# It uses the standard input to place data into the game variables such as x and y.
# YOU DO NOT NEED TO MODIFY THE INITIALIZATION OF THE GAME VARIABLES.


# game loop
while True:
    # x: x position of your pod
    # y: y position of your pod
    # next_checkpoint_x: x position of the next check point
    # next_checkpoint_y: y position of the next check point
    x, y, next_checkpoint_x, next_checkpoint_y = [int(i) for i in input().split()]

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr, flush=True)


    # Edit this line to output the target position
    # and thrust (0 <= thrust <= 100)
    # i.e.: "x y thrust"
    if x < 9000 and y < 4000: # va vers le 1
        print(str(next_checkpoint_x) + " " + str(next_checkpoint_y) + " 100")
    elif x > 10000 and y < 9000: #va vers le 2
        print(str(next_checkpoint_x) + " " + str(next_checkpoint_y) + " 80") 
    elif x < 8000 and y < 7000:
        print(str(next_checkpoint_x) + " " + str(next_checkpoint_y) + " 80")
    else: # va vers le 3
        print(str(next_checkpoint_x) + " " + str(next_checkpoint_y) + " 80")


