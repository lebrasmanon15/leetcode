def myAtoi(s):
    MIN_RANGE = -2**31
    MAX_RANGE = 2**31
    if s :
        s = s.replace(" ", "")
        sign = -1 if s[0] == "-" else 1
        result = ""
        if not s[0].isnumeric() and s[0] not in ["-", "+"]:
            result = 0
        else: 
            for el in s:
                if el.isnumeric():
                    result += el
                elif el in ["-", "+"]:
                    pass
                else:
                    break
            result = int(result) if result else 0
            if result > MAX_RANGE:
                result = MAX_RANGE
            elif result < MIN_RANGE:
                result = MIN_RANGE

        return sign * result
    else:
        return 0
    

print(myAtoi("     +42"))
print(myAtoi("4193 with words"))
