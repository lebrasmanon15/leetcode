def palindromeNumber(x):
    return list(str(x)) == list(str(x))[::-1]

print(palindromeNumber(-121))
print(palindromeNumber(12344321))