def reverseN(x):
    MIN_RANGE = -2**31
    MAX_RANGE = 2**31 -1
    x = str(x)
    result = int("-" + (x[1:][::-1])  if "-" in x else x[::-1])
    final = result if (result > MIN_RANGE and result < MAX_RANGE) else 0
    return final

print(reverseN(-123000000000000000000000000000000000001))
print(reverseN(123))


