from itertools import combinations

scores = {
    ("e", "a", "i", "o", "n", "r", "t", "l", "s", "u") : 1,
    ("d", "g") : 2,
    ("b","c","m","p"): 3,
    ("f","h","v","w","y"): 4,
    ("k") : 5,
    ("j","x") : 8,
    ("q", "z"): 10
}

def score(word):
    score = 0
    for letter in word:
        for k, v in scores.items():
            if letter in k:
                score += v
    return score 

def first_word(dico, possibilities,w):
    for word in dico :
        if ''.join(sorted(word)) in possibilities:
            return word
    
    

n = int(input())
w = {} #word in the dictionnary
word_ordered = []
for i in range(n):
    word = input()
    word_ordered.append(word)
    w[''.join(sorted(word))] = (word, score(word))
letters = input() #letter to play with
r =  len(sorted(w.keys(), key=len)[0])
possibilities = []



for i in range(r,8):
    possibilities.extend(sorted([(''.join(sorted(elem)), score(''.join(sorted(elem)))) for elem in combinations(letters, i) if ''.join(sorted(elem)) in w]))

possibilities.sort(key= lambda x:x[1], reverse=True)

max_point = possibilities[0][1]

possibilities = [elem[0] for elem in possibilities if elem[1] == max_point]

print(first_word(word_ordered, possibilities,w))





""" 10
some
first
potsie
day
could
postie
from
have
back
this
sopitez """