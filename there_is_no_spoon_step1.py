import sys
import math

# Don't let the machines win. You are humanity's last hope...
grid = []
width = int(input())  # the number of cells on the X axis
height = int(input())  # the number of cells on the Y axis
for i in range(height):
    grid.append([input()]) # width characters, each either 0 or .


for line in range(height):
    for node in range(width):
        result = f"{node} {line} "
        if grid[line][0][node] == "0":
            if node != width-1 and "0" in grid[line][0][node+1:]:
                for n in range(node+1, width):
                    if grid[line][0][n] == "0":
                        result += f"{n} {line} "
                        break
            else:
                result += "-1 -1 "
            new_grid =  list(map(list, zip(*grid)))
            if line != height-1:
                test = False
                for l in range(line+1, height):
                    if grid[l][0][node] == "0":
                        result += f"{node} {l} "
                        test = True
                        break
                if test == False:
                    result += "-1 -1 "
            else:
                result += "-1 -1 "
            print(result)
        else:
            pass